#include "Adafruit_MAX31855.h"

Adafruit_MAX31855 max31855(5, 4, 3);

void setup() { 
  Serial.begin(9600);//start serial control to pc
  Serial.print("\n");
  Serial.println("Test temperature");

  max31855.begin();
  
  delay(500);
} 

void loop() {
  double temperature = readTemperature();
  writeTemperature(temperature);
  delay(1000);

}

double readTemperature() {
  return max31855.readCelsius();
}

void writeTemperature(double temperature) {
  Serial.println(temperature);
}

//CLK_pin : 5 : clock
//CS_pin : 4 : chip select
//DO_pin : 3 : data out